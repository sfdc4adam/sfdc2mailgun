/*
 * Name: AccountHandler
 * Object: Account
 * Purpose: Account trigger handler logic
 * Author: Adam Liu 
 * Create Date:  14/04/2020 
 * Modify History: 
 * 14/04/2020   Adam    Version 001
*/
public without sharing class AccountHandler implements ITrigger{
 
    // Constructor
    public AccountHandler(){
    }

    public void bulkBefore(){
    }
 
    public void bulkAfter(){
    }
 
    public void beforeInsert(SObject so){
    }
 
    public void beforeUpdate(SObject oldSo, SObject so){
    }
 
    public void beforeDelete(SObject so){
    }
 
    public void afterInsert(SObject so){
        // load all the in use projects passed to this trigger
		sendEmailByMailgun.sendEmail(Trigger.newMap.keySet(), Trigger.newMap);
    }
 
    public void afterUpdate(SObject oldSo, SObject so){
        // load all the in use projects passed to this trigger
		sendEmailByMailgun.sendEmail(Trigger.newMap.keySet(), Trigger.newMap);
    }
 
    public void afterDelete(SObject so){
    }
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
    }
}