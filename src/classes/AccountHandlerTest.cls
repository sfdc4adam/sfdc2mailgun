@isTest(SeeAllData=true)
private class AccountHandlerTest {

    public static void testData(){
        // Create common test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            Account acc = new Account();
            acc.Name = 'Test00'+i; 
            acc.Email_Address__c = 'Test@test.com';
            testAccts.add(acc);
        }
        insert testAccts;
    }


    @isTest static void testCallMethodFunctionality(){
          Test.startTest();
          testData();
          //Call API Class
          ID acID = [Select Id From Account Where Name =: 'Test001' Limit 1].id; 
          Test.setMock(HttpCalloutMock.class, new TestMockMessageResponse());
          sendEmailByMailgun.sendEmailByCallAPI(acID, 'Email Content', 'Email Subject', 'adamliu2008@gmail.com', 'adamliu2008@gmail.com');
          Test.stopTest();
    }

    @isTest static void testCreateAccount(){
        // Create test accounts
        Test.startTest();
        testData();
        Test.setMock(HttpCalloutMock.class, new TestMockMessageResponse());
        Account testAccts = new Account();
        testAccts.Name = 'Test003';
        testAccts.Email_Address__c = 'Test@test.com';
        insert testAccts;
        Test.stopTest();
    }
    @isTest static void testUpdateAccount(){
        // Update test accounts
        Test.startTest();
        testData();
        Test.setMock(HttpCalloutMock.class, new TestMockMessageResponse());
        Account testAccts = [Select Id, Name From Account Where Name = : 'Test001' Limit 1];
        testAccts.Name = 'Test003';
        Update testAccts;
        Test.stopTest();
    }
    @isTest static void testNegativeTest(){
        // Update test accounts
        Test.startTest();
        testData();
        Test.setMock(HttpCalloutMock.class, new TestMockMessageFailedResponse());
        Account testAccts = [Select Id, Name From Account Where Name = : 'Test001' Limit 1];
        testAccts.Name = 'Test003';
        Update testAccts;
        Test.stopTest();
    }
    @isTest static void testDelTestToCoverage(){
        // Update test accounts
        Test.startTest();
        testData();
        Test.setMock(HttpCalloutMock.class, new TestMockMessageFailedResponse());
        Account testAccts = [Select Id, Name From Account Where Name = : 'Test000' Limit 1];
        Delete testAccts;
        Test.stopTest();
    }
}