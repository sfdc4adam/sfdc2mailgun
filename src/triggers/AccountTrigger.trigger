/*
 * Name: AccountHandler
 * Object: Account
 * Purpose: Account trigger logic
 * Author: Adam Liu 
 * Create Date:  14/04/2020 
 * Modify History: 
 * 14/04/2020   Adam    Version 001
*/
trigger AccountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerFactory.createHandler(Account.sObjectType);
}